# Тестовое задание от WelbeX

 - [x] Инфраструктура находится на AWS и состоит из одного сервера Ubuntu
 - [x] Подняте инстанса с помощью Terraform
 - [x] Поднять сайт в Docker контейнере
 - [] Использование GitLab CI/CD для создание автоматизированного пайплайна по сборке и деплою данного приложения 

 Доп. задание
 - [] Использоватение Ansible для настройки сервера

## В процессе сделано: 
 - Описание среды в Terraform в файле main.tf
 - Установка Docker через terraform 
 - Запуск Docker контенера с сайтом через terraform

## Как проверить:
 - Открыть в браузере 13.51.121.64:8080
